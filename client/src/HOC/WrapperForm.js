import React, { Component } from 'react';
import { validateEmail } from '../helpers/validateForm';

export default function WrapperForm(WrappedComponent) {
    return class WrapperForm extends Component {
        constructor(props) {
            super(props);

            this.state = {
                isFetching: false,
                data: this.props.initialState,
                errors: this.props.requiredFields,
                errorForm: this.props.errorCallback
            };
        }

        componentDidUpdate(prevProps) {
            if (prevProps.errorCallback !== this.props.errorCallback) {
                this.setState({
                    errorForm: this.props.errorCallback
                });
            }
        }

        handleInput = element => {
            const { id, value } = element;
            this.setState(({ data, errors }) => ({
                data: {
                    ...data,
                    [id]: {
                        ...data[id],
                        value: value,
                    }
                },
                errors: this.props.requiredFields,
                errorForm: ''
            }));
        };

        handleSubmit = e => {
            e.preventDefault();
            const { buttonHandler } = this.props;
            const { data, errors } = this.state;
            const isValidate = Object.keys(errors).reduce(
                (sum, item) => sum && this.validateForm(item, data[item].value),
                true
            );
            if (isValidate) {
                console.log('here We Send Data', data);
                buttonHandler && buttonHandler(data);
            } else {
                console.log('no valid data');
            }
        };

        handleBlur = e => {
            const { value, id } = e.currentTarget;
            this.validateForm(id, value);
        };

        validateForm = (id, value) => {
            const isEmpty = this.isNotEmptyValue(id, value);
            switch (true) {
                case id === 'email' && isEmpty:
                    if (!validateEmail(value)) {
                        this.setState(({ errors }) => ({
                            errors: {
                                ...errors,
                                [id]: {
                                    ...errors[id],
                                    value: 'Enter valid email',
                                    type: 'error'
                                }
                            }
                        }));
                        return false;
                    } else if (validateEmail(value)) {
                        this.setState(({ errors }) => ({
                            errors: {
                                ...errors,
                                [id]: {
                                    ...errors[id],
                                    value: '',
                                    type: 'success'
                                }
                            }
                        }));
                        return true;
                    }
                case id === 'confirmPassword' && isEmpty:
                    const {
                        data: { password, confirmPassword }
                    } = this.state;
                    if (password !== confirmPassword) {
                        this.setState(({ errors }) => ({
                            errors: {
                                ...errors,
                                [id]: 'введённые пароли не совпадают'
                            }
                        }));
                        return false;
                    }
                    return true;
                default:
                    return isEmpty;
            }
        };

        isNotEmptyValue = (id, value) => {
            if (!value.trim()) {
                this.setState(({ errors }) => ({
                    errors: {
                        ...errors,
                        [id]: {
                            ...errors[id],
                            value: 'The field must not be empty',
                            type: 'error'
                        }
                    }
                }));
                return false;
            }
            this.setState(({ errors }) => ({
                errors: {
                    ...errors,
                    [id]: {
                        ...errors[id],
                        value: '',
                        type: ''
                    }
                }
            }));
            return true;
        };

        render() {
            return (
                <WrappedComponent
                    {...this.state}
                    {...this.props}
                    handleSubmit={this.handleSubmit}
                    handleBlur={this.handleBlur}
                    handleInput={this.handleInput}
                />
            );
        }
    };
}
