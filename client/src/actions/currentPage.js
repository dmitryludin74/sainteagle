export const SET_CURRENT_TEMPLATE_PAGE = 'SET_CURRENT_TEMPLATE_PAGE';

export const setCurrentTemplate = (linkValue) => {
    return dispatch => {
        dispatch({
            type: SET_CURRENT_TEMPLATE_PAGE,
            payload: linkValue,
        });
    }
}