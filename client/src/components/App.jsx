import React from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import smoothscroll from 'smoothscroll-polyfill';

// Components
import Footer from './Layers/Footer/Footer';
import Home from '../components/Pages/Home/Home';
import AboutUs from '../components/Pages/AboutUs/AboutUs';
import Template from '../components/Pages/Template/Template';
import Menu from './Layers/Menu/Menu';

// Style
import './App.scss'

const onClickHandler = (event) => {
  event.preventDefault();
  let brif = document.getElementById('brif');
  let brifRect = brif.getBoundingClientRect();
  let percent = brifRect.height * 0.1;
  smoothscroll.polyfill();
  window.scroll({ top: brifRect.top - percent + window.pageYOffset, left: 0, behavior: 'smooth' });
}

const App = (props) => {
  return (
    <>
      <div className={`wrap ${props.location.pathname === '/about-us' ? 'wrap-about' : ''}`}>
        <div className="content">
          <Route path="/">
            <Menu onClickHandler={onClickHandler} />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/about-us" component={AboutUs} />
                <Route exact path={`/services/:service`}>
                  <Template onClickHandler={onClickHandler} />
                </Route>
            </Switch>
          </Route>
        </div>
        <Footer />
      </div>
    </>
  )
}

export default withRouter(App);