import React from 'react';

import './Arrow.scss';

const Arrow = ({variables}) => {
    return (
        <div class={`arrow-4 ${variables}`}>
            <span class="arrow-4-left"></span>
            <span class="arrow-4-right"></span>
        </div>
    );
}

export default Arrow;