import React from 'react';

// Components
import Title from '../Title/Title';
import Checkbox from '../Checkbox/Checkbox';
import RadioButton from '../RadioButton/RadioButton';
import Input from '../Form/Input';
import Button from '../Button/Button';

// Style
import './Brif.scss';

const Brif = () => {
    return (
        <div id="brif" className="brif">
            <Title 
                title="ЗАПОЛНИТЕ БРИФ"
                text="Оставьте заявку, или свяжитесь с нами по контактному телефону:" 
                titleStyle="brif__title-style"
                textStyle="brif__text-style"
            />
            <a className="brif__tel" href="tel:+79639964995">+7 (963) 996-49-95</ a>
            <Title 
                subtitle="Выберите тип проекта:"
                subtitleStyle="brif__subtitle-style"
            />
            <div className="brif__wrap">
                <div className="brif__checkboxes">
                    <Checkbox text="Упаковка" />
                    <Checkbox text="Брендинг" />
                    <Checkbox text="Анимация" />
                </div>
                <div className="brif__checkboxes">
                    <Checkbox text="Сайт" />
                    <Checkbox text="Графика" />
                    <Checkbox text="Видео" />

                </div>
                <div className="brif__checkboxes">
                    <Checkbox text="Логотип" />
                    <Checkbox text="Фото" />
                    <Checkbox text="Фирменный стиль" />
                </div>
                <div className="brif__checkboxes">
                    <Checkbox text="Интерьер" />
                </div>
            </div>
            <Title 
                subtitle="Ваш бюджет:"
                subtitleStyle="brif__subtitle-style"
            />
            <div className="brif__wrap">
                <div className="brif__radio">
                    <RadioButton name="price" text="До 250 тыс" />
                    <RadioButton name="price" text="750 тыс - 1 млн" />
                </div>
                <div className="brif__radio">
                    <RadioButton name="price" text="250 тыс - 500 тыс" />
                    <RadioButton name="price" text="1 млн - 5 млн" />

                </div>
                <div className="brif__radio">
                    <RadioButton name="price" text="500 тыс - 750 тыс" />
                    <RadioButton name="price" text="От 5 млн" />
                </div>
            </div>
            <Title 
                subtitle="Опишите задачу:"
                subtitleStyle="brif__subtitle-style"
            />
            <div className="brif__wrap brif__inputs">
                <Input type="textarea" cols="70" placeholder="Нам предстоит" />
            </div>
            <Title 
                subtitle="Контактные данные:"
                subtitleStyle="brif__subtitle-style"
            />
            <div className="brif__wrap brif__inputs">
                <Input type="text" required={true} placeholder="Имя" />
                <Input type="tel" required={true} placeholder="Номер телефона" />
                <Input type="email" required={true} placeholder="E-mail" />
                <Input type="text" placeholder="Город" />
                <Input type="text" placeholder="Компания" />
                <Input type="text" placeholder="Ссылка на сайт" />
                <Input type="textarea" required={true} cols="93" placeholder="Комментарий" />
            </div>
            <div className="brif__button">
                <Button text="ОТПРАВИТЬ" classStyle="btn_theme-main" />
                <p className="brif-button__text">Нажимая на кнопку, вы даете согласие на обработку персональных данных</p>
            </div>
        </div>
    )
}

export default Brif;