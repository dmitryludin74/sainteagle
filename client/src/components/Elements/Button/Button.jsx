import React from 'react';

import './Button.scss';

const Button = ({basicStyle, classStyle, text, onClick, children, icon, type}) => {
    return (
        <button type={type} onClick={onClick} className={`btn ${basicStyle} ${classStyle}`}>
            {icon}
            {text}
            {children}
        </button>
    )
}

export default Button;
