import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Button from './Button';

// styles
import './ButtonUp.scss';

const ButtonUpComponent = ({show, onClick}) => {
    return (
        <React.Fragment>
            <Button href="#" onClick={onClick} basicStyle="btn_theme-main btn_size-medium" classStyle={show ? "scroll-up" : "scroll-up hidden" }>
                <FontAwesomeIcon icon="chevron-up" />
            </Button>
        </React.Fragment>
    )
}

export default ButtonUpComponent;
