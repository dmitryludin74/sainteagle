import React from 'react';

// Components

// Style
import './Checkbox.scss';

const Checkbox = ({text, checked}) => {
    return (
        <label className="checkbox">
            <input defaultValue="false" checked={checked} type="checkbox"/>
            <div className="checkbox__text">{text}</div>
        </label>
    )
}

export default Checkbox;