import React from 'react';
import { NavLink } from 'react-router-dom';

// Components

// Style
import "./Dropmenu.scss";

const links = [
    {title: 'УПАКОВКА', href: '../services/pack'},
    {title: 'САЙТ', href: '../services/site'},
    {title: 'ФИРМЕННЫЙ СТИЛЬ', href: '../services/style'},
    {title: 'БРЕНДИНГ', href: '../services/brand'},
    {title: 'ГРАФИКА', href: '../services/graphics'},
    {title: 'ЛОГОТИП', href: '../services/logo'},
    {title: 'АНИМАЦИЯ', href: '../services/animation'},
    {title: 'ВИДЕО', href: '../services/video'},
    {title: 'ФОТО', href: '../services/photo'},
    {title: 'ИНТЕРЬЕР', href: '../services/interier'},
]

const Dropmenu = ({variables}) => {
  return (
    <nav className="dropmenu">
        <ul className={`dropmenu__list ${variables}`}>
            {links.map((link, index) => (
                <li className="dropmenu__item" key={index* Date.now()}>
                    <NavLink
                        to={link.href}
                        activeClassName="active"
                        className="dropmenu__link"
                    >
                        {link.title}
                    </NavLink>
                </li>
            ))}
        </ul>
    </nav>
  );
}

export default Dropmenu;