import React, { Component } from 'react';
import Input from './Input';
import Button from '../Button/Button';
import Form from './Form';
import WrapperForm from '../../../HOC/WrapperForm';

class FormWrap extends Component {
    render() {
        const {
            data,
            errors,
            handleInput,
            handleSubmit,
            handleBlur,
            formVariants,
            buttonBasicStyle,
            buttonCustomStyle,
            buttonText,
            errorForm
        } = this.props;
        return (    
            <Form errorText={errorForm} customStyle={formVariants} action="#">
                {/* {Object.keys(data).map((el, i) => (
                    data[el].type === "input" 
                    ?   <Input id={data[el].id} messageType={errors[el] && errors[el].type} customMessage={errors[el] && errors[el].value} key={i} onBlur={handleBlur} onChange={handleInput} value={data[el].value} required={data[el].required} basicStyle={data[el].basicStyle} classStyle={data[el].classStyle} name={data[el].name} type={data[el].name} placeholder={data[el].placeholder} />
                    :   data[el].type === "textarea"
                    ?   <textarea value='asd' />
                    :   null
                ))} */}
                <Button onClick={handleSubmit} basicStyle={buttonBasicStyle} classStyle={buttonCustomStyle} type="submit" text={buttonText} />
            </Form>
        )
    }
}

export default WrapperForm(FormWrap);