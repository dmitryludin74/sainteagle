import React from 'react';
import TextareaAutosize from 'react-autosize-textarea';

import './Input.scss';

const Input = ({required, name, id, maxLength, value, placeholder, type, cols}) => {
    return (
        <label className={`input`}>
            {type === "textarea" ? (
                <TextareaAutosize rows="1" cols={cols} required={required} name={name} id={id} maxLength={maxLength} defaultValue={value} className="input__field" placeholder={placeholder} />
            ) : (   
                <input required={required} name={name} id={id} maxLength={maxLength} defaultValue={value} className="input__field" placeholder={placeholder} type={type} />
            )}
        </label>
    )
}

export default Input;
