import React, {useState} from 'react';
import Loader from '../Loader/Loader';

import './Image.scss'

const Image = ({img, format, variables}) => {
    const [loading, setLoading] = useState(false);

    const checkLoad = () => {
        setLoading(true);
    }
    
    return(
        <>
            <img 
                className={`image ${variables}`}
                src={`/images/${img}.${format}`} 
                alt=""
                onLoad={checkLoad}
            />
            {!loading && <Loader />}
        </>
    ) 
            
}

export default Image;