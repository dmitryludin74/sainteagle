import React from 'react';

// Components

// Style
import "./List.scss";

const List = ({variables, lists}) => {
  return (
    <div className="list">
        <ul className={`list__wrap ${variables}`}>
            {lists.map((list, index) => (
                <li className="list__item" key={list.index}>
                    {list.img && <img src={`/images/lists/${list.img}.png`} alt=""/>}
                    <p className="">
                        {list.text}
                    </p>
                </li>
            ))}
        </ul>
    </div>
  );
}

export default List;