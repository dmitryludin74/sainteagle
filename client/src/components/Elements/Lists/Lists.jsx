import React from 'react';

// Components
import Title from '../../Elements/Title/Title';
import List from '../../Elements/Lists/List/List';

// Styles
import './Lists.scss';

const Lists = ({lists, title}) => {
        return (
            <div className="lists">
                <Title subtitle={title} /> 
                <List lists={lists} variables="" />
            </div>
        );
    }

export default Lists;