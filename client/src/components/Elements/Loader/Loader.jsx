import React from 'react';

import './Loader.scss';

export default () => <div className="loader"><div className="lds-facebook"><div></div><div></div><div></div></div></div>;