import React from 'react';
import { NavLink } from 'react-router-dom';

import './Logo.scss'

const Logo = () => {
    return(
        <NavLink className="navbar-brand logo" to="/">
            <img src={`/images/logo.svg`} alt="" />
            <h3 className="logo__text">SAINT EAGLE</h3>
        </NavLink>
    )
}

export default Logo;