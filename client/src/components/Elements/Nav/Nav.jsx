import React from 'react';
import { NavHashLink as NavLink } from 'react-router-hash-link';

// Components
import Arrow from '../Arrow/Arrow';

// Style
import "./Nav.scss";

const links = [
  {
      href: '/about-us',
      text: 'О нас',
  },
  {
      href: '/#services',
      text: 'Услуги',
  },
  {
      text: 'Контакты',
  },
];

const Nav = ({variables, linkStyle, onClickHandler, onClickServicesMenu, arrowStyle}) => {
  return (
    <nav className="nav">
        <ul className={`nav__list ${variables}`}>
            {links.map((link, index) => (
                <li className="nav__item" key={index}>
                    <NavLink
                        exact to={link.href}
                        className="nav__link"
                        activeClassName="nav__link-active"
                        scroll={el => el.scrollIntoView({ behavior: 'smooth', block: 'start' })}
                        onClick={link.text === "Контакты" ? onClickHandler : 
                        (link.text === "Услуги" && window.screen.width < 600) ? onClickServicesMenu 
                        : null}
                    >
                        {link.text}
                        {(link.text === "Услуги") ? (
                            <Arrow variables={arrowStyle} />
                            ) : ( 
                            null
                        )}
                    </NavLink>
                </li>
            ))}
        </ul>
    </nav>
  );
}

export default Nav;
