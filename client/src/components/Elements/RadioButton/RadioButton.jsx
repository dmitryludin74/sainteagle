import React from 'react';

// Components

// Style
import './RadioButton.scss';

const RadioButton = ({text, name}) => {
    return (
        <label className="radio">
            <input defaultValue="" name={name} type="radio"/>
            <div className="radio__text">{text}</div>
        </label>
    )
}

export default RadioButton;