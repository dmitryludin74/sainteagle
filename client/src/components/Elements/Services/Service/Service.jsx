import React from 'react';
import { Link } from 'react-router-dom';

// Components
import Image from '../../Image/Image';
import Title from '../../Title/Title';

// Styles
import './Service.scss';

const Service = ({title, href, img}) => {
        return (
            <Link to={{
                pathname: `/services/${href}`,
                state: {
                    title: title,
                }
            }}
                className="service"
            >
                <Image img={img} format="jpg" variables="service__img" />
                <Title 
                    title={title}
                    text="ПОДРОБНЕЕ"
                    titleStyle="titleStyle"
                    textStyle="textStyle"
                    variables="service-title"
                />
            </Link>
        );
    }

export default Service;