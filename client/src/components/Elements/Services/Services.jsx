import React from 'react';
import Slider from 'react-slick';

// Components
import Service from '../../Elements/Services/Service/Service';
import Title from '../Title/Title';


// Styles
import './Services.scss';

const services = [
    {id: '1', title: 'УПАКОВКА', href: 'pack', img: 'services/pack'},
    {id: '2', title: 'САЙТ', href: 'site', img: 'services/site'},
    {id: '3', title: 'ФИРМЕННЫЙ СТИЛЬ', href: 'style', img: 'services/style'},
    {id: '4', title: 'БРЕНДИНГ', href: 'brand', img: 'services/brand'},
    {id: '5', title: 'ГРАФИКА', href: 'graphics', img: 'services/graphics'},
    {id: '6', title: 'ЛОГОТИП', href: 'logo', img: 'services/logo'},
    {id: '7', title: 'АНИМАЦИЯ', href: 'animation', img: 'services/animation'},
    {id: '8', title: 'ВИДЕО', href: 'video', img: 'services/video'},
    {id: '9', title: 'ФОТО', href: 'photo', img: 'services/foto'},
    {id: '10', title: 'ИНТЕРЬЕР', href: 'interier', img: 'services/interier'},
]

const Services = ({variables}) => {

    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        responsive: [
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 60000,
                dots: true,
                arrows: false,
                dotsClass: 'slick-dots',
              }
            }
        ],
    }

    return (
        <div id="services" className="services">
            <div className="services-text-wrap">
                <p className="label-text">SAINT EAGLE</p>
                <Title 
                    title="МЫ СОЗДАЕМ"
                    titleStyle="title-style"
                />
            </div>
            
            <div className={`services-wrap ${variables}`}>
                {window.screen.width < 600 ? (
                    <Slider {...settings}>
                        {services.map((service) => (
                            <Service 
                                {...service}
                                key={service.id}
                            />
                        ))}
                    </Slider>
                ) : (
                    services.map((service) => (
                        <Service 
                            {...service}
                            key={service.id}
                        />
                    ))
                )}

            </div>
        </div>
            
    );
}
export default Services;