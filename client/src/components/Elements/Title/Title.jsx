import React from 'react';

import './Title.scss';

const Title = ({title, subtitle, text, variables, titleStyle, subtitleStyle, textStyle}) => {
    return (
        <div className={variables}>
            <h2 className={`title ${titleStyle}`}>{title}</h2>
            <h3 className={`subtitle ${subtitleStyle}`}>{subtitle}</h3>
            <p className={`text ${textStyle}`}>{text}</p>
        </div>
    )
}

export default Title;
