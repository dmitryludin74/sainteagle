import React, { Component } from 'react';

// Components
import Portal from '../Portal';

// Styles
import './Tooltip.scss';

class Tooltip extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            styles: {},
        }

        this.refEl = null;
        this.refPopEl = null;
    }

    enterPlaces = () => {
        this.setState({
            show: true
        });
    }

    leavePlaces = () => {
        this.setState({
            show: false
        });

        this.props.callback && this.props.callback();
    }

    setStyles = (el, type) => {
        let elCoord = this.refEl.getBoundingClientRect();
        let elPopCoord = this.refPopEl.getBoundingClientRect();

        const style = {};
        style.left = Math.floor((elCoord.left + elCoord.width / 2) - elPopCoord.width / 2);
        
        if (elCoord.height < elPopCoord.height) {
            style.top = Math.floor(elCoord.top + window.pageYOffset - (elPopCoord.height - elCoord.height) / 2);
        } else {
            style.top = Math.floor(elCoord.top + window.pageYOffset + (elCoord.height - elPopCoord.height) / 2);
        }

        switch (this.props.side) {
            case "left":
                style.left = Math.floor(elCoord.left - this.props.spaces);
                style.transform = 'translateX(-100%)';
                break;
            case "right":
                style.left = Math.floor(elCoord.left + elCoord.width + this.props.spaces);
                style.transform = 'translateX(0%)';
                break;
            case "top":
                    style.top -= this.props.spaces;
                    style.transform = 'translateY(-100%)';
                    break;
            case "bottom":
                    style.top += this.props.spaces;
                    style.transform = 'translateY(100%)';
                    break;
            default:
                break;
        }

        this.setState({
            styles: style
        });
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.refPopEl !== null && this.state.show !== prevState.show) {
            this.setStyles();
        }
    }

    render() {
        return(
            <div ref={ref => this.refEl = ref} onMouseLeave={this.leavePlaces} onMouseEnter={this.enterPlaces}>
                {this.props.children}
                {this.state.show ?
                    <Portal>
                        <div ref={ref => this.refPopEl = ref} className="tooltip-custom" style={this.state.styles}>
                            <div className="tooltip-custom__text">
                                {this.props.text}
                            </div>
                        </div>
                    </Portal>
                    : null
                }
            </div>
        )
    }
}

export default Tooltip;