import React from 'react';

// Components
import Container from '../../blocks/Container/Container';
import Brif from '../../Elements/Brif/Brif';

// Styles
import './Footer.scss';

const Footer = () => {
    return(
        <footer className="footer">
            <Container>
                <Brif />
            </Container>
            <div className="footer__bottom">
                <Container customStyle="container-app">
                    <div className="footer__wrap">
                        <div className="footer__text-wrap">
                            <p className="text"><b>Будем рады вашему визиту к нам по адресу:</b></p>
                            <p className="text">г.Санкт-Петербург, наб. Обводного канала, д. 118АХ, бизнес-центр “Малевич”, офис №9</p>
                        </div>
                        <div className="footer__text-wrap">
                            <p className="text"><b>Наши контакты:</b></p>
                            <p className="text"><b>Телефон:</b> <a className="brif__tel" href="tel:+79995870586">+7(999)587-05-86</a> Александр</p>
                            <p className="text"><b>Почта:</b> <a className="brif__tel" href="mailto:SaintEagle.agency@yandex.ru">SaintEagle.agency@yandex.ru</a></p>   
                        </div>
                        <div className="footer__text-wrap footer__social-wrap">
                            <a target="_blank" className="social inst" href="https://www.instagram.com/saint.eagle.agency/"></a>
                            <a target="_blank" className="social telegram" href="https://tlgg.ru/@sainteagleagency"></a>
                        </div>
                    </div>
                </Container>
            </div>

        </footer>
    )
}

export default Footer;