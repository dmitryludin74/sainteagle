import React, {useState} from 'react';

// Components
import Image from '../../Elements/Image/Image';

// Style
import './AnimationImage.scss';

const AnimationImage = (props) => {
    const {img} = props;
    const [x, setX] = useState(0);
    const [y, setY] = useState(0);
    const [scale, setScale] = useState(1);
    const debounce = (f, ms) => {
        let isCooldown = false;
        return function() {
          if (isCooldown) return;
          f.apply(this, arguments);
          isCooldown = true;
          setTimeout(() => isCooldown = false, ms);
        };
    }
    const mouseOver = () => {
        setScale(0.92)
    }
    const mouseOut = () => {
        setScale(1);
        setX(0);
        setY(0);
    }
    const mouseMoveHandler = debounce((e) => {
        let elemCoord = e.target.getBoundingClientRect();
        setX(((e.pageX - elemCoord.left) - (elemCoord.width/2)) / 40);
        setY(((e.pageY - elemCoord.top) - (elemCoord.height/2)) / 30);
    }, 100)
    return (
        <div 
            onMouseMove={mouseMoveHandler} 
            onMouseOver={mouseOver}
            onMouseOut={mouseOut}
        >
            <span 
                style={{transform: `rotateX(${-y}deg) rotateY(${x}deg) scale(${scale})`}} 
                className="perspective-anim"
            >
                <Image img={img} format="png" />
            </span>
        </div>
    )
}

export default AnimationImage;