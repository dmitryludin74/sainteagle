import React from 'react';

// Components
import Title from '../../Elements/Title/Title';
import AnimationImage from './AnimationImage';
import Image from '../../Elements/Image/Image';

// Style
import './Header.scss';

const Header = (props) => {
    const {title, subtitle, text, button, img, titleStyle, textStyle, video} = props;
    console.log(props);
    return (
        <header className="header">
            <div className="header__content">
                <Title
                    title={title}
                    subtitle={subtitle}
                    text={text}
                    titleStyle={titleStyle}
                    textStyle={textStyle}
                />
                {button && button}
            </div>
            <div className="header__img">
                {window.screen.width >= 600 ? (
                    video ? (
                        <video className="header__video" autoPlay loop>
                            <source src="/videos/moltsshn.mp4" type="video/mp4" />
                        </video>
                    ) : (
                        <AnimationImage img={img} format="png" />
                    )
                ) : (
                    <Image img={img} format="png" />
                )}
            </div>
        </header>
    )
}

export default Header;