import React, {Component} from 'react';

// Components
import Nav from '../../Elements/Nav/Nav';
import Logo from '../../Elements/Logo/Logo';
import Container from '../../blocks/Container/Container';
import Button from '../../Elements/Button/Button';
import Dropmenu from '../../Elements/Dropmenu/Dropmenu';

// Styles
import './Menu.scss';


class Menu extends Component {
    state = {
        menuShow: false,
        servicesShow: false,
    }

    wrapper = React.createRef();
  
    componentWillUnmount() {
      this.removeOutsideClickListener();
    }
    
    addOutsideClickListener() {
      document.addEventListener('click', this.handleDocumentClick);
    }
  
    removeOutsideClickListener() {
      document.removeEventListener('click', this.handleDocumentClick);
    }
    
    onShow() {
      this.addOutsideClickListener();
    }
    
    onHide() {
      this.removeOutsideClickListener();
    }
    
    onClickOutside() {
      this.setState({ menuShow: false });
    }  
  
    handleDocumentClick = e => {
      if (this.wrapper.current && !this.wrapper.current.contains(e.target)) {
        this.onClickOutside();
      }
    };
    
    toggleMenu = () => {
      this.setState(
        prevState => ({ menuShow: !prevState.menuShow, servicesShow: false }),
        () => {
          this.state.menuShow ? this.onShow() : this.onHide();
        },
      );
    };
    
    servicesToggle = (event) => {
        event.preventDefault();
        this.setState({
            servicesShow: !this.state.servicesShow,
        });
    }

    render() {
        const {menuShow, servicesShow} = this.state;

        return (
            <div ref={this.wrapper} className={`menu ${this.props.about ? 'menu-bottom-line' : ''}`}>
                <Container customStyle="container-app">
                    <div className="menu__wrap-left">
                        <Button
                            onClick={this.toggleMenu}
                            classStyle={`burger-menu_btn ${menuShow ? "burger-menu_btn_active" : ""}`} 
                            text={<span></span>}
                        />
                        <Logo logo="logo" />
                    </div>
                    <Nav
                        variables={`nav__list-active ${menuShow ? 'nav__list_show' : ''}`}
                        onClickHandler={this.props.onClickHandler}
                        onClickServicesMenu={this.servicesToggle}
                        arrowStyle={servicesShow ? "open" : " "}
                    />
                    {servicesShow && menuShow && <Dropmenu />}
                </Container>
            </div>
        );
    }
}

export default Menu;