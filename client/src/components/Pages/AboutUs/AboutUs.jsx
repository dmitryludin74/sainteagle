import React from 'react';
import { CSSTransitionGroup } from 'react-transition-group';

// Components
import Header from '../../Layers/Header/Header';
import Title from '../../Elements/Title/Title';
import Image from '../../Elements/Image/Image';
import Lists from '../../Elements/Lists/Lists';
import Container from '../../blocks/Container/Container';

// Style
import './AboutUs.scss';

const lists = [
  {
    text: 'Собственникам небольших производств или крупным отраслевым предприятиям',
    img: 'factory 1'
  },
  {
    text: 'Собственникам компаний, которые продают онлайн или оффлайн',
    img: 'shopping-cart 1'
  },
  {
    text: 'Владельцам ресторанов, кафе, кофеен и пр.',
    img: 'restaurant 1'
  },
  {
    text: 'Предпринимателям, начавшим свой бизнес еще в 90-х, которые до сих пор пользуются устаревшими инструментами',
    img: 'tyrannosaurus-rex 1'
  },
  {
    text: 'Предпринимателям из других сфер, желающим повысить свои финансовые результаты',
    img: 'Ikonka'
  },
];

const AboutUs = () => {
  return (
    <CSSTransitionGroup
      transitionName="example"
      transitionAppear={true}
      transitionAppearTimeout={500}
      transitionEnter={false}
      transitionLeave={false}>
      <div className="about">
        <Header 
          title="О НАС"
          subtitle="Мы - большая команда профессионалов своего дела."
          text="Каждый день работаем для улучшения качества нашего продукта."
          img="about_header"
          titleStyle="about__header-title-style"
          textStyle="about__header-text-style"
        />
        <Container>
          <section className="about__get-up">
            <Title
              variables="about-wrap-title"
              subtitle="Профессионально поднимаем продажи"
              text="Мы делаем дизайн уникальным, чистым и продающим"
              subtitleStyle="about__get-up-subtitle"
              textStyle="text-style about__get-up-text"
            />
            <div className="img-group">
              <Image img="about_1" format="jpg" />
              <Image img="about_2" format="jpg" />
              <Image img="about_3" format="jpg" />
            </div>
          </section>
          <section className="about__behined">
            <Title 
              subtitle="За спиной - огромный опыт ведения бизнеса в различных сферах."
              text="Были провальные кейсы, а есть и успешные , которые существуют и приносят хороший доход по сей день."
            />
            <Title 
              text="Сделав выводы, мы пришли к тому, что устали пользоваться услугами сторонних компаний, которые далеко не всегда в состоянии оказать их на высоком уровне и решили основать свою компанию для помощи таким же предпринимателям, как мы"
            />
          </section>
          <section className="about__who-need-help">
            <Lists lists={lists} title="Кому мы можем помочь :" />
          </section>
        </Container>
      </div>
    </CSSTransitionGroup>
  )
}

export default AboutUs;

