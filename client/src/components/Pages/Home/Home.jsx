import React, { Component } from 'react';
import { CSSTransitionGroup } from 'react-transition-group';
import {Link} from 'react-router-dom';

// Component
import Header from '../../Layers/Header/Header';
import Button from '../../Elements/Button/Button';
import Services from '../../Elements/Services/Services';
import Title from '../../Elements/Title/Title';
import Image from '../../Elements/Image/Image';
import Container from '../../blocks/Container/Container';

// Style
import './Home.scss';

class Home extends Component {
    render() {
        return(
            <CSSTransitionGroup
                transitionName="example"
                transitionAppear={true}
                transitionAppearTimeout={500}
                transitionEnter={false}
                transitionLeave={false}>
                    <div className="home">
                        <Container>
                            <Header
                                video="/videos/moltsshn.mp4"
                                title="SAINT EAGLE"
                                subtitle="DIGITAL AGENCY"
                                text="Мы делаем дизайн уникальным, чистым и продающим"
                                img="home_header"
                                button={
                                    <Link to="/about-us">
                                        <Button text="О нас" classStyle="btn_theme-main" />
                                    </Link>
                                }
                            />
                            <div className="home-block-wrap">
                                <div className="home-text-wrap">
                                    <p className="label-text">SAINT EAGLE</p>
                                    <Title 
                                        title="МЫ НЕ ПРОДАЕМ УСЛУГИ - &emsp; МЫ ЭКОНОМИМ ВАШЕ ВРЕМЯ"
                                        text="Большинство digital-агенств не способны упаковать даже себя, не говоря уже о продуктах заказчиков. Добро пожаловать в нашу реальность: дорого, быстро, надежно." 
                                        titleStyle="title-style"
                                        textStyle="text-style"
                                    />
                                </div>
                                <div className="home-img-wrap">
                                    <Image
                                        img="home-big-img"
                                        format="png"
                                    />
                                </div>
                            </div>
                            <Services />
                        </Container>
                    </div>
            </CSSTransitionGroup>
        )
    }
}

export default Home;
