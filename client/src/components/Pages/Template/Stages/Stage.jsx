import React from 'react';

// Components

// Style

const Stage = ({text, active, out}) => {
  return (
    <li className={`stage ${active ? 'active' : ''} ${out ? 'out' : ''}`}>
        <span className="stage__circle"></span>
        <p className="stage__text">{text}</p>
    </li>
  );
}

export default Stage;