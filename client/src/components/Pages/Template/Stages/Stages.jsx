import React, {Component} from 'react';
// import Slider from 'react-slick';

// Components
import Title from '../../../Elements/Title/Title';
import Stage from './Stage';

// Styles
import './Stages.scss';

class Stages extends Component {

    // settings = {
    //     infinite: true,
    //     speed: 500,
    //     responsive: [
    //         {
    //           breakpoint: 992,
    //           settings: {
    //             slidesToShow: 3,
    //             autoplay: true,
    //             slidesToScroll: 1,
    //             autoplaySpeed: 3000,
    //             dots: false,
    //             arrows: false,
    //             centerMode: true,
    //             infinite: true,
    //           }
    //         }
    //     ],
    // }

    state = {
        count: 0
    }
    componentDidMount() {
        this.addCount = setInterval(() => {
            if (this.state.count >= this.props.stages.arr.length - 1) {
                this.setState({
                    count: 0
                });
            } else {
                this.setState({
                    count: this.state.count + 1
                 });
            }
        }, 4000);
    }

    componentWillUnmount() {
        clearInterval(this.addCount);
    }

    render() {
        const {stages, variables} = this.props;

        return (
            <div className="stages">
                <Title subtitle={stages.title} text={stages.subtitle} />
                <ul className={`stages__wrap ${variables}`}>
                    {/* {window.screen.width < 600 ? (
                        <Slider {...this.settings}>
                            {stages.arr.map((stage, index) => (
                                <Stage out={index + 1 === this.state.count} active={index === this.state.count} key={index} text={stage} />
                            ))}
                        </Slider>
                    ) : ( */}
                            {stages.arr.map((stage, index) => (
                                <Stage out={index + 1 === this.state.count} active={index === this.state.count} key={index} text={stage} />
                            ))}
                    {/* )}    */}
                </ul> 
            </div>
        );
    }
}

export default Stages;