import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import {setCurrentTemplate} from '../../../actions/currentPage';
import {withRouter} from 'react-router-dom';
import { CSSTransitionGroup } from 'react-transition-group';

// Components
import Header from '../../Layers/Header/Header';
import Lists from '../../Elements/Lists/Lists';
import Container from '../../blocks/Container/Container';
import Button from '../../Elements/Button/Button';
import Stages from './Stages/Stages';
import Loader from '../../Elements/Loader/Loader';

// Style
import './Template.scss';

const lists = [
  {
    text: 'Собственникам небольших производств или крупным отраслевым предприятиям',
  },
  {
    text: 'Собственникам компаний, которые продают онлайн или оффлайн',
  },
  {
    text: 'Владельцам ресторанов, кафе, кофеен и пр.',
  },
  {
    text: 'Предпринимателям, начавшим свой бизнес еще в 90-х, которые до сих пор пользуются устаревшими инструментами',
  },
  {
    text: 'Предпринимателям из других сфер, желающим повысить свои финансовые результаты',
  },
];

const Template = (props) => {
    const {page, match, setCurrentTemplateAction, onClickHandler} = props;

    useEffect(() => {
        setCurrentTemplateAction(match.params.service)
    }, [match.params.service]);

    if (!page) {
      return <Loader />
    }

    return (
      <CSSTransitionGroup
      transitionName="example"
      transitionAppear={true}
      transitionAppearTimeout={500}
      transitionEnter={false}
      transitionLeave={false}>
        <div className="template">
            <Container>
                <Header 
                    title={page.header.title}
                    subtitle={page.header.subtitle}
                    text={page.header.text}
                    img={page.header.img}
                    button={
                      <Button text="Заказать" classStyle="btn_theme-main" onClick={onClickHandler} />
                    }
                />
                <section className="stages__lists">
                  <Stages stages={page.stages} />
                </section>
                <section className="template__lists">
                  <Lists lists={page.lists} title="Вы получите:" />
                </section>
            </Container>
        </div>
      </CSSTransitionGroup>
    )
}

const mapStateToProps = ({ pages }) => {
    return {
        page: pages.currentPage,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setCurrentTemplateAction: (link) => dispatch(setCurrentTemplate(link)),
    }
};
  
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Template));

