import React from 'react';

// Style
import './Col.scss'

const Col = (props) => {
    const {children, count} = props;
    return (
        <div className={`col ${count ? 'col-'+count : ''}`}>
            {children}
        </div>
    )
}

export default Col;
