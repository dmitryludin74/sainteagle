import React from 'react';

// Style
import './Container.scss'

const Container = (props) => {
    const {children, fluid, customStyle} = props;
    return (
        <div className={fluid ? 'container-fluid ' : 'container ' + (customStyle ? customStyle : '')}>
            {children}
        </div>
    )
}

export default Container;

