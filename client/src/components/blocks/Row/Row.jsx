import React from 'react';

// Style
import './Row.scss'

const Row = ({children, variables}) => {
  return (
    <div className="row row-align row-just">
      {children}
    </div>
  )
}

export default Row;

