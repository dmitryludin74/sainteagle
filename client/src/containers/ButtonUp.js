import React, { Component } from 'react';
import smoothscroll from 'smoothscroll-polyfill';

// Component
import ButtonUpComponent from '../components/Elements/ButtonUpComponent';

export default class ButtonUp extends Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
        }

        this.flag = true;
    }

    checkScroll = () => {
        window.addEventListener('scroll', () => {
            let scrollTop = window.pageYOffset;

            if (scrollTop >= 400 && this.flag) {
                this.flag = false;
                this.setState({
                    show: true
                });
            } else if (scrollTop < 400 && this.flag === false) {
                this.flag = true;
                this.setState({
                    show: false
                });
            }
        });
    }

    clickHandler = (event) => {
        event.preventDefault();
        smoothscroll.polyfill();
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
    }

    render() {
        this.checkScroll();
        return (
            <React.Fragment>
                <ButtonUpComponent onClick={this.clickHandler} show={this.state.show} />
            </React.Fragment>
        )
    }
}
