import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import {BrowserRouter} from 'react-router-dom';

import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import './index.scss';

import ScrollToTop from './containers/ScrollToTop';
import App from './components/App';

import {store} from './store';


ReactDOM.render(    
<Provider store={store}>
    <BrowserRouter>
        <ScrollToTop>
            <App />
        </ScrollToTop>
    </BrowserRouter>
</Provider>, 
document.getElementById('root')
);
