import { combineReducers } from 'redux';

import pages from './currentPage';

export const rootReducer = combineReducers({
    pages
});